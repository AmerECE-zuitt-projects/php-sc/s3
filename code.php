<?php

class Person
{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName)
    {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName()
    {
        return "Your full name is $this->firstName $this->middleName $this->lastName.";
    }
};

class Developer extends Person
{
    public function printName()
    {
        return "Your full name is $this->firstName $this->middleName $this->lastName, and you're a developer.";
    }
};

class Engineer extends Person
{
    public function printName()
    {
        return "You are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
}

$person = new Person("Amer", "Dan", "Orwa");
$developer = new Developer("Ahmed", "Ivan", "Ala");
$engineer = new Engineer("Meer", "Adel", "Batis");
